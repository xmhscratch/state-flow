const _ = require('lodash')
const uuidV4 = require('uuid')
const hash = require('object-hash')
const traverse = require('traverse')

class StateItem {
    constructor(resolver, stateMemo, keyName, rawState = []) {
        this._id = uuidV4()
        this._key = keyName
        this._rawState = rawState
        this._state = null
        this._memo = stateMemo

        this._resolver = resolver
        return this
    }

    getKey() {
        return this._key
    }

    getId() {
        return this._id
    }

    getStateKey() {
        return hash(this.getStateData())
    }

    getStateData() {
        let results = this.parseState()

        if (results instanceof Error) {
            return {
                error: true,
                message: results.message
            }
        }

        if (this._state) {
            return this._state
        }

        this._state = results
        return results
    }

    parseState() {
        _.templateSettings.interpolate = /{{([\s\S]+?)}}/g

        try {
            let self = this
            let rawState = this._rawState

            const getRefs = function(key) {
                let refs = _.split(key, '@')
                let name = refs[0]
                let type = refs[1]
                return { name, type }
            }

            let state = traverse(rawState).forEach(function (obj) {
                let { type } = getRefs(this.key)
                if (_.isEqual(type, 'raw')) {
                    this.block()
                    return
                }

                if (!_.isObject(obj)) {
                    this.block()
                    return
                }

                obj = _.transform(obj, (result, value, key) => {
                    let { name, type } = getRefs(key)

                    if (_.isEqual(type, 'raw')) {
                        result[name] = value
                        this.block()
                        return
                    }

                    if (_.isObject(value)) {
                        result[name] = value
                        return
                    }

                    let calcValue = _.template(value)(self._memo)
                    switch(type) {
                        default:
                        case 'string':
                            result[name] = calcValue
                            break
                        case 'boolean':
                            let isTrue = (/(true|1)/g).test(calcValue.toLowerCase())
                            result[name] = isTrue ? true : false
                            break
                        case 'integer':
                            result[name] = parseInt(calcValue)
                            break
                    }
                }, {})

                this.update(obj)
            }, {})

            if (false === _.isObject(state)) {
                throw new Error('Wrong state value being setted.')
            }

            return state
        } catch(e) {
            return e
        }
    }

    depend(item) {
        let childId = this.getId()
        let parentId = item.getId()

        this._resolver.setDependency(childId, parentId)
        return this
    }
}

module.exports = StateItem
