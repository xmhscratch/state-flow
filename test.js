const request = require('request')
const StateFlow = require('./index')

let s = new StateFlow((item, done) => {
    let key = item.getStateKey()
    let data = item.getStateData()

    return request(data, (error, resp, body) => {
        if (resp.statusCode !== 200) {
            return done(new Error('not found'))
        }
        return done(error, body)
    })
})

let baseUrl = 'http://127.0.0.1/open-sources/state-flow/samples'
let c1 = s.add('product', {
    'baseUrl': baseUrl,
    'method': 'GET',
    'uri': '/product.json',
    'json@boolean': 'true',
    'headers': {
        'x-test@boolean': 'true'
    }
})

let c2 = s.add('product.options', {
    'baseUrl': baseUrl,
    'method': 'GET',
    'uri': '/product-options-{{ product.sku }}.json1',
    'json@boolean': 'true'
})

let c3 = s.add('product.mediaGalleryEntries', {
    'baseUrl': baseUrl,
    'method': 'GET',
    'uri': '/product-media-galleries-{{ product.sku }}.json',
    'json@boolean': 'true'
})

c3.depend(c2.depend(c1))

s.fetch((error, results) => {
    console.log(error, results)
})
