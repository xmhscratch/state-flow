const _ = require('lodash')
const async = require('async')
const DependencyResolver = require('dependency-resolver')

const StateItem = require('./state-item')

class StateFlow {
    constructor(generator, initialState = {}) {
        this._items = {}
        this._resolver = new DependencyResolver()
        this._generator = generator.bind(this)

        this._errors = []
        this._memo = initialState
        this._omitKeys = _.keys(initialState)

        return this
    }

    add(keyName, state = '') {
        let item = new StateItem(
            this._resolver,
            this._memo,
            keyName, state
        )
        let itemId = item.getId()
        this._items[itemId] = item

        this._resolver.add(itemId)
        return item
    }

    fetch(done) {
        const generateResult = (itemId, callback) => {
            let item = this._items[itemId]
            let stateKey = item.getStateKey()

            return this._generator(item, (error, results) => {
                if (error) {
                    _.set(this._errors, item.getKey(), error.message)
                } else {
                    _.set(this._memo, item.getKey(), results)
                }

                return callback()
            })
        }

        return async.mapSeries(
            this._resolver.sort(),
            async.ensureAsync(generateResult),
            (error) => {
                let results = _.omit(this._memo, this._omitKeys)
                return done(this._errors, results)
            }
        )
    }
}

module.exports = StateFlow
